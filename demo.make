api = 2
core = 7.0

projects[drupal][version] = 7.39

projects[ctools][type] = module
projects[ctools][version] = 1.9

projects[features][type] = module
projects[features][version] = 2.6

projects[libraries][type] = module
projects[libraries][version] = 2.2

projects[mediaelement][version] = "1.2"
projects[mediaelement][type] = "module"

projects[module_filter][version] = "2.0"
projects[module_filter][type] = "module"

projects[scald][version] = 1.4
projects[scald][type] = module

projects[views][type] = module
projects[views][version] = 3.11

projects[wysiwyg][type] = module
projects[wysiwyg][download][type] = git
projects[wysiwyg][download][url] = http://git.drupal.org/project/wysiwyg.git
projects[wysiwyg][download][revision] = 727a20827cfae1a839330cf66eca91420cb1b89b

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.5.3/ckeditor_4.5.3_standard.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

libraries[mediaelement_library][download][type] = get
libraries[mediaelement_library][download][url] = https://github.com/johndyer/mediaelement/archive/2.15.0.zip
libraries[mediaelement_library][directory_name] = mediaelement
libraries[mediaelement_library][type] = library

projects[devel][version] = "1.5"
projects[devel][type] = "module"
